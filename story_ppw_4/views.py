from django.shortcuts import render

def home(request):
    return render(request, "home.html")

def tentang_diriku(request):
    return render(request, "tentang_diriku.html")

def pengalaman(request):
    return render(request, "pengalaman.html")

def under_construction(request):
    return render(request, "under_construction.html")
